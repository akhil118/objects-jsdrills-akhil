
function mapObject(obj, cb) {
  const mappedDict = {}
  for ( let prop in obj ){
    mappedDict[prop] = cb(obj[prop]);
  }
  return mappedDict;
}


module.exports = mapObject;
