function invert(obj) {

    let invertedDict = {};
    
    Object.entries(obj).forEach(entries => {
      let key = entries[0];
      let value = entries[1];
      invertedDict[value] = key
    })

    return invertedDict;
  }

  module.exports = invert; 
